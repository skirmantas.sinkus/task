import React from "react";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    width: '100%'
  },
  formControl: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(8),
    fontWeight: "bold",
    width: '100%',
  },
  underline: {
    borderBottom: '2px solid #e0e0e0',
    '&:before, &:after': {
      borderBottom: '2px solid #e0e0e0',
      content: "none"
    },
  },
}));

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      onValueChange={values => {
        onChange(values.floatValue);
      }}
      thousandSeparator
      decimalScale={2}
      fixedDecimalScale={true}
      allowNegative={false}
      getInputRef={inputRef}
    />
  );
}

NumberFormatCustom.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default function CurrencyInput({ amount, onChange, currency, label }) {
  const classes = useStyles();

  const showError = (!amount && onChange ? true : false);

  return (
    <div className={classes.container}>
      <TextField
        className={classes.formControl}
        label={label}
        value={amount}
        onChange={onChange && onChange}
        helperText={showError && "You must enter amount!"}
        error={showError}
        InputProps={{
          inputComponent: NumberFormatCustom,
          endAdornment: <InputAdornment position="end">{currency}</InputAdornment>,
          classes: {
            underline: classes.underline,
          }

        }}
      />
    </div>
  );
}


CurrencyInput.propTypes = {
  onChange: PropTypes.func
};