import React from 'react';
import { makeStyles } from '@material-ui/styles';
import PropTypes from "prop-types";

import SwapHorizRoundedIcon from '@material-ui/icons/SwapHorizRounded';

const useStyles = makeStyles({
    root: {
        color: "#2196f3",
        display: "flex",
        paddingTop: "2em",
        "&:hover": {
            cursor: "pointer"
        }

    }
});


export default function SwitchButton({ onClick }) {
    const classes = useStyles();
    return (
        <div className={classes.root} onClick={onClick}>
            <SwapHorizRoundedIcon fontSize={"large"} />
        </div>
    )
}

SwitchButton.propTypes = {
    onClick: PropTypes.func
  };



