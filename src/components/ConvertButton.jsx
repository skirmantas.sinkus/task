import React from 'react';
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles({
  root: {
    color: 'white',
    height: 48,
    padding: '0 30px',
    background: '#76ff03',
    "&:hover": {
      background: "#76ff03"
  }
  },
});

export default function ConvertButton({ onClick, disabled}) {
  const classes = useStyles();
  return <Button
    className={classes.root}
    onClick={onClick}
    fullWidth={true}
    variant="contained"
    disabled={disabled}
  >
    Convert
    </Button>;
}

ConvertButton.propTypes = {
  onClick: PropTypes.func.isRequired
};