import React from 'react';
import PropTypes from "prop-types";
import RadioButtonUncheckedRoundedIcon from '@material-ui/icons/RadioButtonUncheckedRounded';
import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles({
    root: {
        fontWeight: "bold",
        marginBottom: "1em"
    },
    icon: {
        position: 'relative',
        top: "0.1em",
        fontWeight: "bold",
        fontSize: "0.8em",
        color: "yellow",
        marginRight: "1em"
    },
    text: {
        fontSize: "0.9em",
        color: "rgba(0, 0, 0, 0.54)"
    }
});



export default function RateInfo({ show, fromCurrency, toCurrency, rate }) {
    const classes = useStyles();

    if (!show) return null;

    return <React.Fragment>
        <div className={classes.root}>
            <RadioButtonUncheckedRoundedIcon fontSize="small" className={classes.icon} />
            <span>
                1 {fromCurrency} = {rate} {toCurrency}
            </span>
        </div>
        <span className={classes.text}>
            All figures are lived mid-market rates, which are for informational purposes only.
            To see the rates for money transfer, please select sending money option.
        </span>

    </React.Fragment>

};


RateInfo.propTypes = {
    show: PropTypes.bool,
    fromCurrency: PropTypes.string,
    toCurrency: PropTypes.string,
    rate: PropTypes.number
};