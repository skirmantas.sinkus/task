import React from 'react';
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import 'currency-flags/dist/currency-flags.min.css';

import * as currencies from '../currency/currencies';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(4),
        minWidth: '100%',
        fontWeight: "bold",
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
        borderBottom: '1px solid #e0e0e0',
        '&:before, &:after': {
          borderBottom: '1px solid #e0e0e0',
          content: "none"
        },
        
    },
}));

export default function CurrencySelect({ label, currency, onChange }) {
    const classes = useStyles();
    return (
        <FormControl className={classes.formControl}>
            <InputLabel shrink htmlFor="age-label-placeholder">
                {label}
            </InputLabel>
            <Select
                value={currency}
                onChange={onChange}
                displayEmpty
                name="currency" 
                className={classes.selectEmpty}
            >
                {Object.keys(currencies).map((key, index) =>
                    (<MenuItem key={index} value={key}>
                        <React.Fragment><div className={`currency-flag currency-flag-${key.toLowerCase()}`}></div>&nbsp;{key} </React.Fragment>
                    </MenuItem>))}
            </Select>
        </FormControl>
    );
}


CurrencySelect.propTypes = {
    onChange: PropTypes.func.isRequired,
    currency: PropTypes.string,
    label: PropTypes.string
  };