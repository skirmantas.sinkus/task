import axios from 'axios';


const receiveData = (data) => ({
    type: 'RECEIVE_DATA',
    data
})

const setIsLoading = (state) => ({
    type: 'SET_IS_LOADING',
    state
})

const setIsError = (state) => ({
    type: 'SET_IS_ERROR',
    state
})

export const setFromCurrency = (currency) => ({
    type: 'SET_FROM_CURRENCY',
    currency
})

export const setToCurrency = (currency) => ({
    type: 'SET_TO_CURRENCY',
    currency
})

export const setAmount = (amount) => ({
    type: 'SET_AMOUNT',
    amount
})

export const switchCurrencies = () => ({
    type: 'SWITCH_CURRENCIES',
})


export const fetchRate = (url) => {
    return async dispatch => {
        dispatch(setIsLoading(true));
        try {
            const result = await axios.get(url);
            dispatch(setIsLoading(false));
            dispatch(receiveData(result.data));
        } catch (e) {
            dispatch(setIsLoading(false));
            dispatch(setIsError(true));
            throw e
        }


    }
}