const initialState = {
    data: {},
    isLoading: false,
    isError: false,
    fromCurrency: "EUR",
    toCurrency: "GBP",
    amount: 1
}


export default (state = initialState, action) => {
    switch (action.type) {
        case 'RECEIVE_DATA':
            return {
                ...state,
                data: action.data,
            };
        case 'SET_IS_LOADING':
            return {
                ...state,
                isLoading: action.state,
            };
        case 'SET_IS_ERROR':
            return {
                ...state,
                isError: action.state,
            };
        case 'SET_FROM_CURRENCY':
            return {
                ...state,
                fromCurrency: action.currency,
            };
        case 'SET_TO_CURRENCY':
            return {
                ...state,
                toCurrency: action.currency,
            };
        case 'SET_AMOUNT':
            return {
                ...state,
                amount: action.amount,
            };
        case 'SWITCH_CURRENCIES':
            console.log("labas");
            return {
                ...state,
                fromCurrency: state.toCurrency,
                toCurrency: state.fromCurrency
            };
        default:
            return state;
    }
};