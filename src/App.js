import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { fetchRate, setFromCurrency, setToCurrency, setAmount, switchCurrencies } from './store/actions';
import { useSelector, useDispatch } from 'react-redux';

import CurrencyInput from './components/CurrencyInput';
import CurrencySelect from './components/CurrencySelect';
import SwitchButton from './components/SwitchButton';
import ConvertButon from './components/ConvertButton';
import RateInfo from './components/RateInfo';

import { Container } from '@material-ui/core';

function App() {
  const [convert, setConvert] = useState(false);
  const dispatch = useDispatch();
  const state = useSelector(state => state.currency);

  useEffect(() => {
    if (convert) {
      if (state.amount) {
        dispatch(fetchRate(`https://my.transfergo.com/api/fx-rates?from=${state.fromCurrency}&to=${state.toCurrency}&amount=${state.amount}`));
      }
    }
  }, [state.amount, state.fromCurrency, state.toCurrency, convert]);



  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <Container maxWidth="sm">

        <div className="wrapper">
          <div className="columns">
            <div className="from">
              <CurrencySelect
                label="FROM:"
                currency={state.fromCurrency}
                onChange={event => dispatch(setFromCurrency(event.target.value))}
              />
              {convert && <CurrencyInput
                onChange={value => dispatch(setAmount(value))}
                amount={state.amount}
                label={"AMOUNT:"}
                currency={state.fromCurrency}
              />}
            </div>

            <SwitchButton onClick={() => dispatch(switchCurrencies())} />

            <div className="to">
              <CurrencySelect
                label="TO:"
                currency={state.toCurrency}
                onChange={event => dispatch(setToCurrency(event.target.value))}
              />
              {convert &&
                <CurrencyInput
                  amount={state.data ? state.data.toAmount : state.amount}
                  label={"COVERTED TO:"}
                  currency={state.toCurrency}
                />}
            </div>
          </div>

        </div>

        {!convert &&
          <React.Fragment>
            <br></br>
            <CurrencyInput
              onChange={value => dispatch(setAmount(value))}
              amount={state.amount}
              label={"AMOUNT:"}
              currency={state.fromCurrency}
              fullWidth={true}
            />
            <br></br>
            <ConvertButon onClick={() => setConvert(true)} disabled={!state.amount} />
          </React.Fragment>
        }

        <RateInfo
          rate={state.data.rate}
          fromCurrency={state.fromCurrency}
          toCurrency={state.toCurrency}
          show={convert}
        />
      </Container>
    </div>
  );
}
export default App;
